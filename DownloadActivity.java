public class DownloadActivity extends Activity implements OnClickListener{  

         public static final String downloadUrl = "http://gdown.baidu.com/data/wisegame/60288c8f92238775/FruitNinja_1809.apk";         

         private Button start;
         private int totalFileLength;
         private int downloadLength;
         private ProgressBar pbar1;
         private View mView;
         private AlertDialog alertDialog;
         private TextView tv_progress; 

         @Override
         protected void onCreate(Bundle savedInstanceState) {
                   super.onCreate(savedInstanceState);
                   setContentView(R.layout.download_main);
                   start = (Button) findViewById(R.id.download);
                   start.setOnClickListener(this);
         }

         @Override
         public void onClick(View v) {
                   switch(v.getId()) {
                   case R.id.download:
                            showProgressBar();
                            download(downloadUrl);
                            break;
                   }           
         }

         public void download(final String url) {
                   if (url == null) return;
                   new Thread(new Runnable() {                       
                            @Override
                            public void run() {
                                     try {
                                               URL mURL = new URL(url);
                                               HttpURLConnection conn = (HttpURLConnection)mURL.openConnection();  
                                               conn.setConnectTimeout(100000);
                                               conn.connect();
                                               totalFileLength = conn.getContentLength();                                            
                                               if (totalFileLength <= 0) {
                                                        return;
                                               }                                           
                                               pbar1.setMax(100);                                            
                                               InputStream is = conn.getInputStream();
                                               File dir = new File("/sdcard/barond"); // need to check if SD card exists
                                               if (!dir.exists()) {
                                                        dir.mkdirs();
                                               }
                                               String fileName = downloadUrl.substring(downloadUrl.lastIndexOf("/") + 1);
                                               File file = new File(dir + "/" +fileName);
                                               byte[] b = new byte[1024];
                                               FileOutputStream fos = new FileOutputStream(file);
                                               int c = 0;
                                               int count = 0;
                                               while ((c = is.read(b)) != -1) {
                                                        fos.write(b, 0, c);
                                                        count += c;
                                                        downloadLength = (count * 100) / totalFileLength;
                                                        Message msg = new Message();
                                                        msg.what = 88;
                                                        mHandler.sendMessage(msg);
                                               }
                                               fos.flush();
                                               is.close();
                                               fos.close();
                                               mHandler.sendEmptyMessage(10001);
                                     } catch (Exception e) {
                                               e.printStackTrace();
                                     }
                            }
                   }).start();
         }
         
         private Handler mHandler = new Handler() {
                   public void handleMessage(Message msg) {
                            if (msg.what == 10001) {
                                     alertDialog.dismiss();
                            }
                            if (msg.what == 88) {
                                     pbar1.setProgress(downloadLength);
                                     tv_progress.setText(downloadLength + "%");
                            }
                   }
         };
       
         public void showProgressBar() {
                   mView = LayoutInflater.from(DownloadActivity.this).inflate(R.layout.progressbar, null);
                   pbar1 = (ProgressBar) mView.findViewById(R.id.pbar1);
                   pbar1.setVisibility(View.VISIBLE);
                   tv_progress = (TextView) mView.findViewById(R.id.tv_progress);          
                  AlertDialog.Builder builder = new AlertDialog.Builder(DownloadActivity.this);
                   builder.setTitle(R.string.app_name)
                   .setView(mView);
                   alertDialog = builder.create();
                   alertDialog.setCanceledOnTouchOutside(false);
                   alertDialog.show();
         } 
}

//ProgressBar布局文件
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical" >
     <ProgressBar 
         android:id="@+id/pbar1"
         android:layout_width="match_parent"
         android:layout_height="wrap_content"
         android:max="100"
         style="@android:style/Widget.ProgressBar.Horizontal"
         android:visibility="gone" />
     <TextView 
         android:id="@+id/tv_progress"
         android:layout_marginLeft="10dp"
         android:layout_width="wrap_content"
         android:layout_height="wrap_content" />       
</LinearLayout>
 
//download_main.xml布局文件
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="fill_parent"
    android:layout_height="fill_parent"
    android:orientation="vertical">  
    <Button
        android:id="@+id/download"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content"
        android:text="Start Download" />
</LinearLayout>
